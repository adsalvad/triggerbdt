Test to produce a pandas file from a root file and train a BDT using sklearn

Use the `setup.sh` when starting SWAN
* Points to my /eos/ python setup, you can install locally the packages you will use (pandas, numpy, matplotlib, sklearn...)

`createHDF.py` creates the `pandas_trigger.h5` from a Tau Trigger sample using root_pandas
* See root_pandas documentation to see everything you can do!
* In the example the option flatten is to have one entry per track instead of the default entry per event

'TMVAPlots.py' has some plot function definitions, better to have it in a general script

The python notebook loads the pandas file, splits it into train and testing samples, trains a BDT and outputs some AUC values, the ROC curve and the score 
____________________________________________________________

Packages needed to work on SWAN
*  Keras
*  tables

Packages needed to create the HDF file
```
setupATLAS
lsetup "lcgenv -p LCG_94python3 x86_64-centos7-gcc7-opt ROOT"
lsetup "lcgenv -p LCG_94python3 x86_64-centos7-gcc7-opt root_numpy"
lsetup "lcgenv -p LCG_94python3 x86_64-centos7-gcc7-opt pandas"
```

if you need to install anything use pip3, (tables, root_pandas...)
if you don't have pip3 set it up with:
`lsetup "lcgenv -p LCG_94python3 x86_64-centos7-gcc7-opt pip"`