export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
#lsetup "lcgenv -p LCG_94 x86_64-centos7-gcc7-opt pandas"
#lsetup "lcgenv -p LCG_94 x86_64-centos7-gcc7-opt root_numpy"
#lsetup "lcgenv -p LCG_94 x86_64-centos7-gcc7-opt numexpr"
#lsetup "lcgenv -p LCG_94 x86_64-centos7-gcc7-opt ipython"
lsetup hdf5
export PYTHONPATH=/eos/user/a/adsalvad/python_pipnew/lib/python3.6/site-packages/:$PYTHONPATH
